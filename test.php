<?php

require __DIR__.'/vendor/autoload.php';

$aggregator = new \SearchAggregator\Aggregator();
$aggregator->addWrapper(new \SearchAggregator\Wrapper\Google());
$aggregator->addWrapper(new \SearchAggregator\Wrapper\Bing());
print_r($aggregator->search('google  maps'));