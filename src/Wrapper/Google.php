<?php

namespace SearchAggregator\Wrapper;


use GuzzleHttp\Psr7\Response;
use SearchAggregator\ResultItem;
use Symfony\Component\CssSelector\CssSelectorConverter;

class Google extends AbstractWrapper
{
    /**
     * {@inheritdoc}
     */
    protected function getBaseUrl()
    {
        return 'https://google.com/search';
    }

    /**
     * {@inheritdoc}
     */
    protected function parseResponse(Response $response)
    {
        $data = $response->getBody()->getContents();

        $dom = new \DOMDocument();
        @$dom->loadHTML($data);
        $xPath = new \DOMXPath($dom);

        $converter = new CssSelectorConverter();
        $nodes = $xPath->query($converter->toXPath('#ires .g'));

        $result = array();
        foreach ($nodes as $node) {
            $title = $xPath->query($converter->toXPath('.r'), $node)->item(0)->nodeValue;
            $url = trim($xPath->query($converter->toXPath('cite'), $node)->item(0)->nodeValue, '/');
            $result[] = new ResultItem($title, $url, array('google'));
        }

        return $result;
    }
}