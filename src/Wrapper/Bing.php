<?php

namespace SearchAggregator\Wrapper;


use GuzzleHttp\Psr7\Response;
use SearchAggregator\ResultItem;
use Symfony\Component\CssSelector\CssSelectorConverter;

class Bing extends AbstractWrapper
{
    /**
     * {@inheritdoc}
     */
    protected function getBaseUrl()
    {
        return 'https://www.bing.com/search';
    }

    /**
     * {@inheritdoc}
     */
    protected function parseResponse(Response $response)
    {
        $data = $response->getBody()->getContents();

        $dom = new \DOMDocument();
        @$dom->loadHTML($data);
        $xPath = new \DOMXPath($dom);

        $converter = new CssSelectorConverter();
        $nodes = $xPath->query($converter->toXPath('ol#b_results li.b_algo'));

        $result = array();
        foreach ($nodes as $node) {
            $title = $xPath->query($converter->toXPath('h2'), $node)->item(0)->nodeValue;
            $url = $xPath->query($converter->toXPath('.b_attribution cite'), $node)->item(0)->nodeValue;
            $result[] = new ResultItem($title, $url, array('bing'));
        }

        return $result;
    }
}