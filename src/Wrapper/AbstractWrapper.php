<?php

namespace SearchAggregator\Wrapper;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use SearchAggregator\ResultItem;

abstract class AbstractWrapper
{
    /**
     * @return string
     */
    protected abstract function getBaseUrl();

    /**
     * @param Response $response
     *
     * @return ResultItem[]
     */
    protected abstract function parseResponse(Response $response);

    /**
     * @return string
     */
    protected function getParamName()
    {
        return 'q';
    }

    /**
     * @param $query
     *
     * @return ResultItem[]
     */
    public function search($query)
    {
        $paramName = $this->getParamName();
        $client = new Client(array('base_uri' => $this->getBaseUrl()));
        $response = $client->request('GET', '', array('query' => array($paramName => $query)));

        return $this->parseResponse($response);
    }
}