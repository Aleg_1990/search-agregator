<?php

namespace SearchAggregator;

use SearchAggregator\Wrapper\AbstractWrapper;

class Aggregator
{
    /** @var AbstractWrapper[] */
    private $wrappers = array();

    public function addWrapper(AbstractWrapper $wrapper)
    {
        if (!in_array($wrapper, $this->wrappers)) {
            $this->wrappers[] = $wrapper;
        }
    }

    /**
     * @param string $query
     *
     * @return ResultItem[]
     */
    public function search($query)
    {
        $result = array();
        foreach ($this->wrappers as $wrapper) {
            $result = $this->addResults($result, $wrapper->search($query));
        }

        return $result;
    }

    private function addResults(array $destination, array $results)
    {
        foreach ($results as $result) {
            if(count($destination) === 0) {
                $destination[] = $result;
                continue;
            }
            $exists = false;
            foreach ($destination as &$destResult) {
                if ($result->getUrl() === $destResult->getUrl()) {
                    $exists = true;
                    $sources = $result->getSources();
//                    if(in_array($sources[0], $destResult->getSources())) {
                        $destResult->addSource($sources[0]);
//                    }

                    continue;
                }
            }

            if (!$exists) {
                $destination[] = $result;
            }
        }
        return $destination;
    }
}