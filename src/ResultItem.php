<?php

namespace SearchAggregator;

class ResultItem
{
    /** @var string */
    private $title;

    /** @var string */
    private $url;

    /** @var string[] */
    private $sources = array();

    /**
     * ResultItem constructor.
     * @param string $title
     * @param string $url
     * @param string[] $sources
     */
    public function __construct($title, $url, array $sources)
    {
        $this->title = $title;
        $this->url = $url;
        $this->sources = $sources;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return string[]
     */
    public function getSources()
    {
        return $this->sources;
    }

    public function addSource($source)
    {
        $this->sources[] = $source;
    }
}